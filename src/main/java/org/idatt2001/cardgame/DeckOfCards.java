package org.idatt2001.cardgame;

import java.util.ArrayList;

/**
 * Deck of Cards class
 * Creates a deck of 52 PlayingCards
 */
public class DeckOfCards {
    /**
     * The Deck.
     */
    ArrayList<PlayingCard> deck = new ArrayList<>();
    private final char[] suit = { 'S', 'H', 'D', 'C' };


    /**
     * Instantiates a new Deck of 52 PlayingCards.
     */
    public DeckOfCards() {
        for (char suits:suit) {
            for (int i = 1; i < 14; i++) {
                deck.add(new PlayingCard(suits,i));
            }
        }
    }

    /**
     * Number of cards in deck
     * @return number of cards int
     */
    public int cardsInDeck(){
        return deck.size();
    }

    /**
     * Gets card.
     *
     * @param position the position of card in deck
     * @return the card at position in deck
     * @throws IndexOutOfBoundsException the index out of bounds exception
     */
    public PlayingCard getCard(int position) throws IndexOutOfBoundsException{
        if(position<deck.size()) {
            return deck.get(position);
        } else{
            throw new IndexOutOfBoundsException("There is no card at that position");
        }

   }

    /**
     * Remove card from deck
     * @param position the position
     */
    public void removeCard(int position){
        deck.remove(position);
    }

    /**
     * Deal hand array list.
     * @param n number of cards dealt
     * @return list of random cards from deck
     * @throws IllegalArgumentException the illegal argument exception
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException{
        if(n<=deck.size()) {
            ArrayList<PlayingCard> dealtCards = new ArrayList<>();
            for (int i = 1; i <= n; i++) {
                int randomNumber = (int) Math.floor(Math.random() * deck.size());
                dealtCards.add(getCard(randomNumber));
                removeCard(randomNumber);
            }
            return dealtCards;
        }else{
            throw new IllegalArgumentException("There are not enough cards in deck");
        }
    }
}
