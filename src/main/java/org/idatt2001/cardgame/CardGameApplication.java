package org.idatt2001.cardgame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class CardGameApplication extends Application {
    public static Stage stage;
    @Override
    public void start(Stage stage) throws Exception {
        CardGameApplication.stage = stage;
        stage.getIcons().add(new Image(CardGameApplication.class.getResourceAsStream("Images/2_of_H.png")));
        FXMLLoader fxmlLoader = new FXMLLoader(CardGameApplication.class.getResource("cardgame-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(),1200,600);
        stage.setTitle("JavaFX Scene");
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }


}
