package org.idatt2001.cardgame;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.media.AudioClip;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;


public class CardGameController  {
    AudioClip sound = new AudioClip(this.getClass().getResource("Audio/shuffling-cards-4.mp3").toString());
    @FXML
    public HBox hbox;

    @FXML
    public Button shuffleButton;

    @FXML
    public Text handText;

    @FXML
    public Text cardOfHearts;

    @FXML
    public Text flushBool;

    @FXML
    public Text queenOfSpadesBool;

    @FXML
    public Text sumOfFaces;

    @FXML
    public Text cardsLeft;

    @FXML
    public Button checkHand;

    @FXML
    public Button dealHand;
    private  DeckOfCards deck = new DeckOfCards();
    private  HandOfCards hand;

    @FXML
    public void initialize() {

    }

    @FXML
     public void checkHandButtonClicked() {
        if (hand !=null) {
            sumOfFaces.setText(String.valueOf(hand.SumCards()));
            if (hand.checkForFlush()) {
                flushBool.setText("True");
            } else {
                flushBool.setText("False");
            }
            if (hand.checkForSpadeQueen()) {
                queenOfSpadesBool.setText("True");
            } else {
                queenOfSpadesBool.setText("False");
            }
            ;
            cardOfHearts.setText(hand.showHearts().toString());
        }
    }
    @FXML
    public void dealHandButtonClicked(ActionEvent event){
        if(deck.cardsInDeck()>4) {
            sound.play();
            hand = new HandOfCards(deck.dealHand(5));
            hbox.getChildren().removeAll(hbox.getChildren());
            for (PlayingCard card:hand.getHand()) {
                ImageView imageView =  new ImageView(new Image(CardGameController.class.getResourceAsStream(card.getImage())));
                imageView.setPreserveRatio(true);
                imageView.setFitWidth(150);
                hbox.getChildren().add(imageView);
            }
            cardsLeft.setText("" + deck.cardsInDeck());
        }
        else{
            cardsLeft.setText("There are not enough cards left to deal: " + deck.cardsInDeck());
        }
    }

    @FXML
    public void shuffleButtonClicked(ActionEvent event){
        sound.play();
        deck = new DeckOfCards();
        hand = null;
        hbox.getChildren().removeAll(hbox.getChildren());
        flushBool.setText("---");
        queenOfSpadesBool.setText("---");
        cardOfHearts.setText("---");
        cardsLeft.setText(""+deck.cardsInDeck());
        sumOfFaces.setText("---");
    }

}

