package org.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Hand of cards class
 * Playingcards that will be checked for card combinations
 */
public class HandOfCards {
    /**
     * The Hand.
     */
    ArrayList<PlayingCard> hand;
    private final char[] suit = { 'S', 'H', 'D', 'C' };

    /**
     * Instantiates a new Hand of cards.
     * @param hand the hand
     */
    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Gets new cards for hand
     * @param cards list of cards added
     */
    public void getNewCards(ArrayList<PlayingCard> cards) {
        hand.addAll(cards);
    }

    /**
     * Get list of cards in hand
     * @return the array list
     */
    public ArrayList<PlayingCard> getHand(){
        return hand;
    }

    /**
     * Sum of cards int.
     * @return the int
     */
    public int SumCards(){
        int sum = 0;
        for (PlayingCard card:hand){
            sum = sum+card.getFace();
        }
        return sum;
    }

    /**
     * Show cards that are hearts
     * @return String of cards
     */
    public String showHearts(){
        ArrayList<PlayingCard> listOfHearts = new ArrayList<>();
        listOfHearts.addAll(hand.stream().filter(c->c.getSuit()==('H')).toList());
        if(listOfHearts.size()==0){
            return "There are no hearts";
        }
        return listOfHearts.toString();
    }

    /**
     * Check for spade queen boolean.
     * @return the boolean
     */
    public Boolean checkForSpadeQueen(){
        return hand.stream().anyMatch(s->s.getFace()==12 && s.getSuit()=='S');
    }

    /**
     * Check for flush boolean.
     * @return the boolean
     */
    public boolean checkForFlush(){
        for (char suits: suit) {
            if(hand.stream().distinct().allMatch(p->p.getSuit()==suits))
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return ""+hand;
    }
}
