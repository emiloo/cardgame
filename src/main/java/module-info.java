module org.idatt.cardgame {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.media;
    exports org.idatt2001.cardgame;
}