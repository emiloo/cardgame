import org.idatt2001.cardgame.DeckOfCards;
import org.idatt2001.cardgame.HandOfCards;
import org.idatt2001.cardgame.PlayingCard;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


public class HandOfCardsTest {
    private DeckOfCards deck;
    private HandOfCards hand;
    private ArrayList<PlayingCard> newCards;

    @BeforeEach
    @DisplayName("Create deck")
    public void testData(){
        deck = new DeckOfCards();
        hand = new HandOfCards(deck.dealHand(0));
        newCards = new ArrayList<>();
        newCards.add(new PlayingCard('S', 10));
        newCards.add(new PlayingCard('H', 5));
        hand.getNewCards(newCards);
    }

    @Test
    @DisplayName("Get new Cards")
    public void getNewCardsTest(){
    ArrayList<PlayingCard> newCards = new ArrayList<>();
    newCards.add(new PlayingCard('S', 1));
    hand.getNewCards(newCards);
    Assertions.assertEquals(3,hand.getHand().size());
    }

    @Test
    @DisplayName("Sum of Cards")
    public void sumOfCardsTest(){
        Assertions.assertEquals(15,hand.SumCards());
    }

    @Test
    @DisplayName("Show Hearts")
    public void showHeartsTest(){
        Assertions.assertEquals("[H5]",hand.showHearts());
    }

    @Test
    @DisplayName("Show no Hearts")
    public void showNoHeartsTest(){
        hand = new HandOfCards(deck.dealHand(0));
        newCards = new ArrayList<>();
        newCards.add(new PlayingCard('S', 10));
        newCards.add(new PlayingCard('C', 5));
        hand.getNewCards(newCards);
        Assertions.assertEquals("There are no hearts",hand.showHearts());
    }

    @Test
    @DisplayName("Check for Spade Queen")
    public void checkForSpadeQueenTest(){
        hand = new HandOfCards(deck.dealHand(0));
        newCards = new ArrayList<>();
        newCards.add(new PlayingCard('G', 10));
        newCards.add(new PlayingCard('S', 12));
        hand.getNewCards(newCards);
        Assertions.assertTrue(hand.checkForSpadeQueen());
    }

    @Test
    @DisplayName("Check for no Spade Queen")
    public void checkForNoSpadeQueenTest(){
        hand = new HandOfCards(deck.dealHand(0));
        newCards = new ArrayList<>();
        newCards.add(new PlayingCard('G', 10));
        newCards.add(new PlayingCard('S', 11));
        hand.getNewCards(newCards);
        Assertions.assertFalse(hand.checkForSpadeQueen());
    }

    @Test
    @DisplayName("Check for Flush")
    public void checkForFlushTest(){
        hand = new HandOfCards(deck.dealHand(0));
        newCards = new ArrayList<>();
        newCards.add(new PlayingCard('S', 10));
        newCards.add(new PlayingCard('S', 12));
        newCards.add(new PlayingCard('S', 1));
        newCards.add(new PlayingCard('S', 13));
        newCards.add(new PlayingCard('S', 6));
        hand.getNewCards(newCards);
        Assertions.assertTrue(hand.checkForFlush());
    }

    @Test
    @DisplayName("Check for no Flush")
    public void checkForNoFlushTest(){
        hand = new HandOfCards(deck.dealHand(0));
        newCards = new ArrayList<>();
        newCards.add(new PlayingCard('S', 10));
        newCards.add(new PlayingCard('S', 12));
        newCards.add(new PlayingCard('S', 1));
        newCards.add(new PlayingCard('S', 13));
        newCards.add(new PlayingCard('H', 6));
        hand.getNewCards(newCards);
        Assertions.assertFalse(hand.checkForFlush());
    }




}
