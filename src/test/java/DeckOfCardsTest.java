
import org.idatt2001.cardgame.DeckOfCards;
import org.idatt2001.cardgame.HandOfCards;
import org.idatt2001.cardgame.PlayingCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


public class DeckOfCardsTest {

    @Test
    void createADeck(){
        DeckOfCards deck = new DeckOfCards();
        Assertions.assertEquals(52,deck.cardsInDeck());
    }

    @Test
    void getCardAtPosition10(){
        DeckOfCards deck = new DeckOfCards();
        PlayingCard spade11 = deck.getCard(10);
        Assertions.assertEquals(spade11, new PlayingCard('S',11));
    }
    @Test
    @DisplayName("Get Card Outside deck")
    void getCardAtPosition52(){
        DeckOfCards deck = new DeckOfCards();
        PlayingCard cardOutsideRange = new PlayingCard('S',1);
        try {
            cardOutsideRange = deck.getCard(52);
        }catch (IndexOutOfBoundsException e){
        }
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> deck.getCard(52));
        Assertions.assertEquals(new PlayingCard('S', 1), cardOutsideRange);
    }

    @Test
    void dealCards(){
        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = new HandOfCards(deck.dealHand(5));
        Assertions.assertEquals(5,hand.getHand().size());
    }

    @Test
    @DisplayName("Try to deal more cards than in deck")
    void dealMoreCardsThanInDeck(){
        DeckOfCards deck = new DeckOfCards();
        Assertions.assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53));
    }
}